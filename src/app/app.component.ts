import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'bookApp';
  public isCollapsed = false;

  // This function will toggle our display. 
  // isCollapsed is property binded (Angular topic: Property Binding)
  toogleCollapsed(){
    this.isCollapsed = !this.isCollapsed;
    console.log("collapse toggled!")
  }
}
