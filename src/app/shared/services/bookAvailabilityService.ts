import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class BookAvailabilityService {

    // the constructor contains code that we want to initialize first before the entire class can load/page render. 
    // Basically, this part has the 1st priority before the UI/page loads
    constructor(
        private http: HttpClient,
    ) {
    }

    // This is similar to "fetch" but in HTTP request format (best practice)
    getBookData(data: any) {
        return this.http.get<any[]>("https://jsonblob.com/api/456479a7-302f-11ea-893e-9d0b7a7dd72d", {});
    }
}
