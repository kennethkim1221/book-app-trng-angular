import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BookAvailabilityService } from '../shared/services/bookAvailabilityService';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  public bookArray: any[] = [];
  public isDataLoaded: boolean = false;

  // Subscription variables. 
  // This variable will hold instances of streams/services
  private getBookDataService: Subscription;

  // (COMPONENT LIFE CYCLE HOOK) This function will run FIRST before any other defined functions within this class
  constructor(private BookAvailabilityService: BookAvailabilityService) {
  }

  // (COMPONENT LIFE CYCLE HOOK) This functions will run SECOND after the constructor 
  ngOnInit() {
    this.fetchBookData().then(() => {
      console.log(this.bookArray);
      this.isDataLoaded = true;
    });
  }

  // This fucntion uses a promise that contains an HTTP call/service. 
  // The response of the HTTP request will contain the mockdata obtained from the endpoint
  // A promise must be "resolve" or "reject"
  // Basically, promises are asynchronus functions
  fetchBookData() {
    return new Promise(function (resolve, reject) {
      console.log("Fetching book data");

      // getBookDataService is a subscription variable. We use subscription variable to be able to open & close streams
      // BookAvailabilityService is an Injectable class that contains the HTTP request. We cann the method defined within the class
      // to open a stream
      this.getBookDataService = this.BookAvailabilityService.getBookData().subscribe(
        (data) => {
          data.data.forEach(element => {
            this.bookArray.push(element)
          });
          resolve("");
        },
        (error) => {
          console.log("ERROR");
          reject("");
        }
      )
    }.bind(this));
  }

  // (COMPONENT LIFE CYCLE HOOK) (BEST PRACTICE) Runs when the current component is removed from the DOM 
  // Or basically when the component is closed
  // This is typically ommitted as this is an optional function
  // However, problems can arise if we did not "close" the subscription stream
  ngOnDestroy() {
    if (this.getBookDataService) { this.getBookDataService.unsubscribe() }
  }
}
