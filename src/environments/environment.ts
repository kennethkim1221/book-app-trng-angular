// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  jsonURL: "https://jsonblob.com/api/456479a7-302f-11ea-893e-9d0b7a7dd72d'"
};
